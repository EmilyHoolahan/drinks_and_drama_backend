"use strict";
var __awaiter =
    (this && this.__awaiter) ||
    function (thisArg, _arguments, P, generator) {
        function adopt(value) {
            return value instanceof P
                ? value
                : new P(function (resolve) {
                    resolve(value);
                });
        }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) {
                try {
                    step(generator.next(value));
                } catch (e) {
                    reject(e);
                }
            }
            function rejected(value) {
                try {
                    step(generator["throw"](value));
                } catch (e) {
                    reject(e);
                }
            }
            function step(result) {
                result.done
                    ? resolve(result.value)
                    : adopt(result.value).then(fulfilled, rejected);
            }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    };

var __importDefault =
    (this && this.__importDefault) ||
    function (mod) {
        return mod && mod.__esModule ? mod : { default: mod };
    };

Object.defineProperty(exports, "__esModule", { value: true });

exports.createUser = exports.getUser = void 0;

const express_1 = __importDefault(require("express"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const jwtSecret = require("../constants");
const saltRounds = require("../constants");
const seqModels_1 = require("../other_services/models/seqModels");
const sequelize_1 = require("sequelize");
const sequalizerConnection_1 = __importDefault(
    require("../other_services/sequalizerConnection")
);
const dotenv_1 = __importDefault(require("dotenv"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));

dotenv_1.default.config();
console.log(saltRounds.saltRounds);
console.log(jwtSecret.jwtSecret);
const router = express_1.default.Router();
router.use(express_1.default.json());
router.post("/auth/login", async (req, res) => {
    console.log(req.body.user_username);
    console.log(req.body.user_password);
    try {
        const userExist = await seqModels_1.User.findOne({
            where: { user_username: req.body.user_username },
        });
        if (!userExist) {
            // Email doesn't exist
            return res.status(400).json({ error: "Wrong credidentials" });
        }

        //      const result = await getUser(req.body.user_username, req.body.user_password);

        // Check if passwords match
        const isPasswordValid = await bcrypt_1.default.compare(
            req.body.user_password,
            userExist.user_password
        );
        if (!isPasswordValid) {
            return res.status(401).send("Invalid credentials");
        }

        // Remove sensitive information from JWT payload
        const jwtUser = {
            user_id: req.body.user_id,
            user_username: req.body.user_username,
            user_email: req.body.user_email,
            user_img: req.body.user_img,
        };

        // Use environment variable for JWT secret
        const authToken = jsonwebtoken_1.default.sign(
            { user: jwtUser },
            jwtSecret.jwtSecret
        );
        const resultWithToken = {
            authToken: authToken,
            user: jwtUser,
        };

        res.status(200).send(resultWithToken);
        console.log("logged in");
    } catch (error) {
        console.log(error);
        res.status(401).send("Something went wrong with user login");
    }
});
router.post("/auth/signup", async (req, res) => {
    console.log(req.body);

    try {
        const result = await createUser(
            req.body.user_username_sig,
            req.body.user_password_sig,
            req.body.user_email_sig
        );

        // Check if the username already exists

        const username = await createUser(
            req.body.user_username_sig,
        );
        /* const alreadyExists = await seqModels_1.User.findOne({
            where: { user_username: username },
        });

        if (alreadyExists) {
            let takenUsername = {
                takenUsername: "Username already in use",
            }
            res.json(takenUsername);
            throw { code: 409, message: "Username already in use" };

        } */

        let jwtUser = {
            user_id: result.user_id,
            user_username: result.user_username,
            user_email: result.user_email,
            user_img: result.user_img,
        };

        let resultWithToken = {
            authToken: jsonwebtoken_1.default.sign(
                { user: jwtUser, expiresIn: "7d", algorithm: "HS256" },
                jwtSecret.jwtSecret
            ),
            user: result,
        };



        res.status(200).json(resultWithToken);
    } catch (error) {
        if (error.code == 409) {
            res.status(409).send(error.message);
        } else {
            res.status(500).send("Something went wrong while creating user");
        }
        throw error;
    }
});

router.post("/auth/verify", (req, res) =>
    __awaiter(void 0, void 0, void 0, function* () {
        try {
            let decodedUser = jsonwebtoken_1.default.verify(
                req.body.authToken,
                jwtSecret.jwtSecret
            );
            const result = yield getUser(
                decodedUser.user.user_username,
                decodedUser.user.user_password
            );
            res.status(200).send(result);
        } catch (error) {
            throw error;
        }
    })
);

function getUser(user_username, user_password) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const user = yield seqModels_1.User.findOne({
                where: { user_username, user_password },
                limit: 1,
            });
            if (!user) {
                throw new Error("No user found with that Username");
            } else if (
                !bcrypt_1.default.compareSync(user_password, user.user_password)
            ) {
                throw new Error("Incorect Password");
            } else {
                return user; // Remember to remove the password from the returned user object
            }
        } catch (error) {
            throw error;
        }
    });
}
exports.getUser = getUser;

async function createUser(
    user_username_sig,
    user_password_sig,
    user_email_sig
) {
    try {
        if (!user_password_sig) {
            throw new Error("Password is missing");
        }

        // Check if the username already exists
        const alreadyExists = await seqModels_1.User.findOne({
            where: { user_username: user_username_sig },
        });

        if (alreadyExists) {

            throw { code: 409, message: "Username already in use" };

        }

        const hash_password_sig = bcrypt_1.default.hashSync(
            user_password_sig,
            saltRounds.saltRounds
        );

        const createdUser = await seqModels_1.User.create({
            user_username: user_username_sig,
            user_password: hash_password_sig,
            user_email: user_email_sig,
        });

        return createdUser;
    } catch (error) {
        console.error(error);
        throw error;
    }
}
exports.createUser = createUser;

exports.default = router;

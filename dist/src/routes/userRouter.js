"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUserById = exports.getAllUsers = void 0;
const express_1 = __importDefault(require("express"));
const mysqlConnSetup_1 = __importDefault(require("../db_services/mysqlConnSetup"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const seqModels_1 = require("../other_services/models/seqModels");
const winstonLogger_1 = __importDefault(require("../other_services/winstonLogger"));
const router = express_1.default.Router();
router.use(express_1.default.json());

// getUserById
router.get("/user/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield getUserById(req.params.id);
        if (result.length == 0) {
            res.status(404).send("No user found with the given id");
        }
        else {
            res.status(200).send(result);
        }
    }
    catch (err) {
        console.log(err);
        res.status(500).send("Something went wrong with fetching user");
    }
}));
function getUserById(id) {
    return __awaiter(this, void 0, void 0, function* () {
        const connection = yield mysqlConnSetup_1.default.getConnection();
        try {
            const [rows] = yield connection.query(`CALL get_user_information(?)`, [id]);
            connection.release();
            return rows;
        }
        catch (err) {
            console.log("no user found with the given ID: ", err);
            connection.release();
        }
    });
}
exports.getUserById = getUserById;

// createUser

router.post("/user", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield createUser(req.body);
        res.status(200).json(result);
    } catch (error) {
        console.log(error);
        res.status(500).json("Internal server error");
    }
}));
function createUser(value) {
    return __awaiter(this, void 0, void 0, function* () {
        value.user_password = yield bcrypt_1.default.hash(value.default.getConnection());
        try {
            yield connection.query(`CALL create_user(?,?,?,?)`, [value.user_id, value.user_username, value.user_password, value.user_email]);
            connection.release();
            return "User added";
        } catch (error) {
            console.log(error);
            connection.release();
        }
    })
}
exports.createUser = createUser;

// updateUser
router.post("/user/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        req.body.user_password = yield bcrypt_1.default.hash(req.body.user_password, 10)
        const result = yield updateUser(req.params.id, req.body)
        res.status(200).json(result)

    } catch (error) { throw error }
}))
function updateUser(user_id, values) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield connection.query(`CALL update_user(?,?,?,?)`, [values.user_id, values.user_username, values.user_password, values.user_email])
            connection.release()
            return "user updated"
        } catch (error) {
            console.log(error);
            connection.release()
            throw error
        }
    })
}
exports.updateUser = updateUser

exports.default = router;

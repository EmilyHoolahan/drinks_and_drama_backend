var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
  function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
    function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
    function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* exports.createCard = exports.getCardsUpToFinishRange = */
exports.getCardById = exports.getAllCards = void 0;
const express_1 = __importDefault(require("express"));
const winstonLogger_1 = __importDefault(
  require("../other_services/winstonLogger")
);
const seqModels_1 = require("../other_services/models/seqModels");
const sequelize_1 = require("sequelize");
const sequalizerConnection_1 = __importDefault(require("../other_services/sequalizerConnection"))
const router = express_1.default.Router();
router.use(express_1.default.json());

// ---------------------- Get all cards ----------------------
router.get("/cards", (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const result = yield getAllCards();
      res.status(200).send(result);
    } catch (error) {
      winstonLogger_1.default.error(
        "Error getting all cards: [getAllCards, 1]",
        error);
      res.status(500).send(error);
    }
  })
);
function getAllCards() {
  return __awaiter(this, void 0, void 0, function* () {
    try {
      const cards = yield seqModels_1.Card.findAll();
      const cardsArray = cards.map((card) => card.toJSON());
      return cardsArray;
    } catch (error) {
      winstonLogger_1.default.error("Error getting getAllCards", error);
      throw error;
    }
  });
}
exports.getAllCards = getAllCards;


router.get("/card/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
  try {
    const result = yield getCardById(req.params.id);
    res.status(200).send(result)
  } catch (error) {
    winstonLogger_1.default.error("Error getting getCardById", error)
    res.status(500).send(error)
  }
}));

function getCardById(id) {
  return __awaiter(this, void 0, void 0, void 0, function* () {
    try {
      const results = yield sequalizerConnection_1.default.query(`CALL get_card_by_gem(?)`, {
        replacement: [id],
        type: seqModels_1.QueryTypes.RAW,
        model: seqModels_1.Card,
      });
      if (!results) {
        winstonLogger_1.default.error("no cards were found with given id");
        throw new Error;
      }
    } catch (error) {
      winstonLogger_1.default.error(`Error`, error);
      throw error;
    }
  })
}
exports.getCardById = getCardById;
exports.default = router;
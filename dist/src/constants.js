require('dotenv').config();

const jwtSecret = process.env.JWT_SECRET || "drinks_n_drama343433";
const saltRounds = parseInt(process.env.SALT_ROUNDS, 10);

exports.jwtSecret = jwtSecret
exports.saltRounds = saltRounds


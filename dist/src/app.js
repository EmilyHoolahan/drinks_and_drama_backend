var __importDefault = (this && this.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};


var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const sequalizerConnection_1 = require("./other_services/sequalizerConnection");
const cardsRouter_1 = __importDefault(require("./routes/cardsRouter"));
const usersRouter_1 = __importDefault(require("./routes/userRouter"));
const authRouter_1 = __importDefault(require("./routes/authRouter"));
const winstonLogger_1 = __importDefault(
  require("./other_services/winstonLogger")
);
const dotenv_1 = __importDefault(require("dotenv"));
const cors_1 = __importDefault(require("cors"));
dotenv_1.default.config();

const app = (0, express_1.default)();
app.use((0, cors_1.default)());

// API routes imported from routes folder
app.use(cardsRouter_1.default);
app.use(usersRouter_1.default);
app.use(authRouter_1.default);

// --- auth and sync sequelize
(0, sequalizerConnection_1.sequalizeAuth)();
(0, sequalizerConnection_1.sequelizeSync)();

// --- Do this when the server is closed
process.on("SIGINT", () => {
  winstonLogger_1.default.end();
  console.log("Cheers!");
  process.exit(0);
});

const port = (_a = process.env.PORT) !== null && _a !== void 0 ? _a : 3000;
app.listen(port, () => {
  /* console.log(`App is listening on ${port}`); */
  console.log(`Server now running on port ${port}`);
});

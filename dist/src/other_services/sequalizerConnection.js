var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.sequelizeSync = exports.sequalizeAuth = void 0;
const sequelize_1 = require("sequelize");
const dotenv_1 = __importDefault(require("dotenv"));
const winstonLogger_1 = __importDefault(require("./winstonLogger"));
dotenv_1.default.config();
const sequelize = new sequelize_1.Sequelize(
  process.env.DB_NAME,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    host: "localhost",
    port: Number(process.env.DB_PORT) || 3306,
    dialect: "mysql",
    logging: (msg) => {
      winstonLogger_1.default.verbose(`Received log message: ${msg}`);
    },
  }
);
const sequalizeAuth = () =>
  __awaiter(void 0, void 0, void 0, function* () {
    sequelize
      .authenticate()
      .then(() =>
        winstonLogger_1.default.verbose(
          "Connection has been established successfully."
        )
      )
      .catch((error) =>
        winstonLogger_1.default.error(
          "Unable to connect to the database:",
          error
        )
      );
  });
exports.sequalizeAuth = sequalizeAuth;
const sequelizeSync = () =>
  __awaiter(void 0, void 0, void 0, function* () {
    yield sequelize
      .sync()
      .then(() => winstonLogger_1.default.verbose("Sequelize sync successful"))
      .catch((error) =>
        winstonLogger_1.default.error("Sequelize sync failed", error)
      );
  });
exports.sequelizeSync = sequelizeSync;
exports.default = sequelize;

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};

Object.defineProperty(exports, "__esModule", { value: true });
exports.Card = exports.User = void 0;
const sequelize_1 = require("sequelize");
const sequalizerConnection_1 = __importDefault(require("../sequalizerConnection"));

class Card extends sequelize_1.Model { }
exports.Card = Card;
Card.init({
    card_id: {
        type: sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    card_title: {
        type: sequelize_1.DataTypes.STRING(45),
        allowNull: false,
    },
    card_description: {
        type: sequelize_1.DataTypes.STRING(500),
        allowNull: false,
    },
    card_img: {
        type: sequelize_1.DataTypes.STRING(45),
        allowNull: false,
    },
    card_gem_id: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
    }
}, {
    sequelize: sequalizerConnection_1.default,
    modleName: 'Card',
    tableName: 'cards',
    timestamps: false,
    createdAt: false,
});

class User extends sequelize_1.Model { }
exports.User = User;
User.init({
    user_id: {
        type: sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    user_username: {
        type: sequelize_1.DataTypes.STRING(45),
        allowNull: false,
    },
    user_password: {
        type: sequelize_1.DataTypes.STRING(100),
        allowNull: false,
    },
    user_email: {
        type: sequelize_1.DataTypes.STRING(100),
        allowNull: false,
    },
    user_img: {
        type: sequelize_1.DataTypes.STRING(20),
        allowNull: true,
    }
}, {
    sequelize: sequalizerConnection_1.default,
    modleName: 'User',
    tableName: 'users',
    timestamps: false,
    createdAt: false,
});
